# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp import api, tools

class product_template(osv.osv):
    _inherit = "product.template"
    
    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.website_image, avoid_resize_medium=True)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

    
    _columns = {
            'page':fields.char('Website Page'),
            'sequence_page':fields.char('Sequence in page'),
            'website_image': fields.binary("Image website"),
                    }
    
    