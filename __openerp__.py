{
"name": "Tuscanysoul soul",
"version": "1.0",
"depends":['website','website_sale','website_forum','website_blog','website_crm'],
"author": "Matteo Casavecchia",
'category': 'Theme/Services',
"description": """ tuscany soul:""",

"init_xml":[],
'data': [
         #'security/ir.model.access.csv',
         #'view/product_view.xml',
         'views/home.xml',
         'views/page2.xml',
         'views/layout.xml',
         'static/src/view/assets.xml',
         
           ],
'demo': [],
'test': [],
'installable': True,
 'images': [
        'static/description/icon.png',
    ],

}
